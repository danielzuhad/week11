const db = require("../models");
const todo = db.todo;
class TodoRepository {
  async getAllTodos() {
    return todo.findAll();
  }

  async getTodoById(id) {
    return todo.findByPk(id);
  }

  async createTodo(todoData) {
    return todo.create(todoData);
  }

  async updateTodo(id, todoData) {
    const todo = await todo.findByPk(id);
    if (!todo) {
      throw new Error("Todo not found");
    }

    return todo.update(todoData);
  }

  async deleteTodo(id) {
    const todo = await todo.findByPk(id);
    if (!todo) {
      throw new Error("Todo not found");
    }

    return todo.destroy();
  }
}

module.exports = new TodoRepository();
