const request = require("supertest");
const app = require("../index");

let createdTodoId;
describe("GET /ping", () => {
  it("should return 200 OK", async () => {
    const response = await request(app).get("/ping");
    expect(response.status).toBe(200);
    expect(response.text).toBe("Pong!");
  });
});

describe("GET /todo", () => {
  it("should return 200 ok", () => {
    request(app)
      .get("/todo")
      .expect("Content-Type", "application/json; charset=utf-8")
      .expect(200);
  });
});

describe("POST /todo", () => {
  // Test case
  it("should return 200 ok", () => {
    request(app)
      .post("/todo")
      .set({
        title: "Lorem",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing",
      })
      .expect("Content-Type", "application/json; charset=utf-8")
      .expect(201);
  });
});

describe("GET /todo/:id", () => {
  it("should return 200 ok", () => {
    request(app)
      .get("/todo")
      .set({
        title: "Lorem",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing",
      })
      .expect("Content-Type", "application/json; charset=utf-8")
      .expect(200);

    request(app)
      .get(`/api/todos/1`)
      .expect("Content-Type", "application/json; charset=utf-8")
      .expect(200);
  });

  it("should return 404 not found", () => {
    request(app)
      .get(`/api/todos/2000`)
      .expect("Content-Type", "application/json; charset=utf-8")
      .expect(404);
  });
});

describe("PUT /todo/:id", () => {
  it("should return 200 ok", () => {
    request(app)
      .put("/todo")
      .set({
        title: "Lorem",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing",
      })
      .expect("Content-Type", "application/json; charset=utf-8")
      .expect(201);

    request(app)
      .get(`/api/todo/1`)
      .expect("Content-Type", "application/json; charset=utf-8")
      .expect(200);
  });

  it("should return 404 if todo not found", async () => {
    const updatedTodoData = {
      title: "Updated Todo",
      description: "Updated description",
    };

    await request(app)
      .put("/todo/nonexistent-id")
      .send(updatedTodoData)
      .expect("Content-Type", /json/)
      .expect(404);
  });
});

describe("DELETE /todo/:id", () => {
  it("should return 200 ok", () => {
    request(app).delete(`/todo/1`).expect(204);
  });

  it("should return 404 if todo not found", async () => {
    const nonExistentTodoId = "non-existent-id";

    await request(app).delete(`/todo/${nonExistentTodoId}`).expect(400);
  });
});
