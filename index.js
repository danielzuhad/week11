const express = require("express");
const app = express();
const todoRoute = require("./routes/todo.route");
const bodyParser = require("body-parser");
app.use(bodyParser.json());

app.get("/ping", (req, res) => {
  res.send("Pong!");
});
app.use("/todo", todoRoute);

module.exports = app;
