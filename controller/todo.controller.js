const todoRepository = require("../repository/todo.repository");

class TodoController {
  async getAllTodos(req, res) {
    try {
      const todos = await todoRepository.getAllTodos();
      res.status(200).json(todos);
    } catch (error) {
      res.status(400).json({ error: error.message });
    }
  }

  async getTodoById(req, res) {
    const { id } = req.params;
    try {
      const todo = await todoRepository.getTodoById(id);
      if (!todo) {
        return res.status(404).json({ error: "Todo not found" });
      }
      res.json(todo);
    } catch (error) {
      res.status(400).json({ error: error.message });
    }
  }

  async createTodo(req, res) {
    const todoData = req.body;
    try {
      const todo = await todoRepository.createTodo(todoData);
      res.status(201).json(todo);
    } catch (error) {
      res.status(400).json({ error: error.message });
    }
  }

  async updateTodo(req, res) {
    const { id } = req.params;
    const todoData = req.body;
    try {
      const todo = await todoRepository.updateTodo(id, todoData);
      res.status(201).json(todo);
    } catch (error) {
      res.status(404).json({ error: error.message });
    }
  }

  async deleteTodo(req, res) {
    const { id } = req.params;
    try {
      await todoRepository.deleteTodo(id);
      res.sendStatus(204);
    } catch (error) {
      res.status(400).json({ error: error.message });
    }
  }
}

module.exports = new TodoController();
